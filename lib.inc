section .text
 %define EXIT_SYSCALL 60
 %define WRITE_SYSCALL 1
 %define READ_SYSCALL 0
 %define STDOUT 1
 %define STDIN 0
 %define NEWLINE_SYMBOL 0xA
 %define ZERO_SYMBOL 0x30
 %define MINUS_SYMBOL 0x2D
 %define SPACE_SYMBOL 0x20
 %define TAB_SYMBOL 0x9

 
; Принимает код возврата и завершает текущий процесс
exit:
    ;xor rax, rax,
    ;ret 
    mov  rax, EXIT_SYSCALL                    
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        jz .done
        inc rax
        jmp .loop
    .done
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    mov rdx, rax
    mov rax, WRITE_SYSCALL 
    mov rdi, STDOUT
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    mov rdx, 1 ;byte count
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_SYMBOL
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp
    mov r9, 10
    mov rax, rdi
    dec rsp;
	mov byte[rsp], 0
    .loop:
        xor rdx, rdx
        div r9
        add rdx, ZERO_SYMBOL
        dec rsp
        mov [rsp], dl
        test rax, rax
        jz .done
        jmp .loop
    .done:
        mov rdi, rsp
        call print_string
        mov rsp, r8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .done
    neg rdi
    push rdi
    mov rdi, MINUS_SYMBOL
    call print_char
    pop rdi
    .done:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    call string_length
    mov rsi, rax
    call string_length
    cmp rax, rsi
    jne .not_equal
    pop rsi
    pop rdi
    .loop:
        mov r8b, [rdi]
        mov r9b, [rsi]
        cmp r8b, r9b
        jne .not_equal
        test r8b, r9b
        jz .equal
        inc rdi
        inc rsi
        jmp .loop
    .not_equal:
        xor rax, rax
        ret
    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rax, READ_SYSCALL
    mov rdi, STDIN
    mov rdx, 1 ;byte count
    mov rsi, rsp
    syscall
    pop rax
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor r10, r10
    mov r8, rdi
    mov r9, rsi
    .skip_space:
        call read_char
        cmp rax, SPACE_SYMBOL
        je .skip_space
        cmp rax, TAB_SYMBOL
        je .skip_space
        cmp rax, NEWLINE_SYMBOL
        je .skip_space
    .loop:
        cmp rax, SPACE_SYMBOL
        je .done
        cmp rax, TAB_SYMBOL
        je .done
        cmp rax, NEWLINE_SYMBOL
        je .done
        test rax, rax
        je .done
        cmp r9, r10
        je .error
        mov byte[r8+r10], al
        inc r10
        call read_char
        jmp .loop
    .done:
        mov byte[r8+r10], 0
        mov rax, r8
        mov rdx, r10
        ret
    .error:
        mov rax, 0
        ret 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r9, r9
    mov r8, 10
    .loop:
        mov r9b, byte[rdi + rdx]
        ;-------------------------------------------
        ;area check
        cmp r9b, '0'
        jl .done
        cmp r9b, '9'
        jg .done
        ;-------------------------------------------        
        sub r9b, '0' ; from ASCII
        push rdx
        mul r8
        pop rdx
        inc rdx
        add rax, r9
        jmp .loop
    .done:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    xor r9, r9
    mov r9b, byte[rdi]
    cmp r9b, MINUS_SYMBOL
    jnz .done
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
    .done:
        call parse_uint
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r9, r9
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    cmp rax, rdx
    jge .error
    .loop:
        mov al, byte[rdi + r9]
        mov byte[rsi + r9], al
        inc r9
        test al, al
        jne .loop
        mov rax, r9
        ret
    .error:
        xor rax, rax
        ret
